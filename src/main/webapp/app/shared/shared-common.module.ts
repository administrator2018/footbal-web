import { NgModule } from '@angular/core';

import { FootballwebSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [FootballwebSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [FootballwebSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class FootballwebSharedCommonModule {}
