/**
 * View Models used by Spring MVC REST controllers.
 */
package com.football.web.web.rest.vm;
